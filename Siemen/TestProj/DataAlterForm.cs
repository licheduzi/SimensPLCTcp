﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TestProj
{
    public partial class DataAlterForm : Form
    {
        public double DataValue
        {
            get
            {

                return Convert.ToDouble(tbxValue.Text.Trim());
            }
            set
            {

                tbxValue.Text = value.ToString();
            }
        }



        public DataAlterForm()
        {
            InitializeComponent();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {

            if (!isNumberic(tbxValue.Text))
            {
                MessageBox.Show("请输入数值");
                return;
            }
            this.DialogResult = DialogResult.OK;
        }


        private bool isNumberic(string message)
        {

            if (message == "")
            {
                return false;
            }
            else
            {
                System.Text.RegularExpressions.Regex m_regex = new System.Text.RegularExpressions.Regex("^(-?[0-9]*[.]*[0-9]{0,5})$");
                return m_regex.IsMatch(message);
            }

        }


        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}
